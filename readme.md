# QuizMan 
QuizMan is an quiz app which you play together with your friends on a single device, and take turn to answer a question. For every consectuive question that is answered correct,
a point is added to the pot. The first player to answer incorrectly gets their score deducted by the current number of points in the pot.

# Motivation, why did we develop this app?
This app was developed as an project in the course iOS-development, where the task was to write an app of your choice for iOS. We chose to make a quiz, because we felt that it had
a good mix of learning how to use an API, using TableViews, constraints and animations to improve the user experience.

# API
We chose to use the opentDB API for our quiz app. It featured a good variety of questions and difficult, with an easy-to-use API Helper to customize what kind of questions you wanted.
Another reason why, and the most important, was that it is free to use!