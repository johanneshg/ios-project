//
//  ViewController.swift
//  ProjectWorkiOS
//
//  Created by Johannes Hou Gustafsson on 2018-11-09.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var PlayButton: UIButton!
    @IBOutlet weak var RuleButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PlayButton.layer.cornerRadius = 15
        RuleButton.layer.cornerRadius = 15

        // Do any additional setup after loading the view, typically from a nib.
    }


    
    
}

