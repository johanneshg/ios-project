//
//  ThirdViewController.swift
//  ProjectWorkiOS
//
//  Created by Johannes Hou Gustafsson on 2018-11-09.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    @IBOutlet weak var answer1: UIButton!
    @IBOutlet weak var answer2: UIButton!
    @IBOutlet weak var answer3: UIButton!
    @IBOutlet weak var answer4: UIButton!
    @IBOutlet weak var questionWindow: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        answer1.layer.cornerRadius = 25
        answer2.layer.cornerRadius = 25
        answer3.layer.cornerRadius = 25
        answer4.layer.cornerRadius = 25


        
        // Do any additional setup after loading the view.
    }
    



}
