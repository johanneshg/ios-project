//
//  AddPlayerViewController.swift
//  ProjectWorkiOS
//
//  Created by Jakob Hjalmarsson on 2018-11-21.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

let defaults = UserDefaults(suiteName: "com.ProjectWorkIOS")

class AddPlayerViewController: UIViewController {
    

    @IBOutlet weak var startGame: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var rows = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APIHandler.getQuestions()
        startGame.layer.cornerRadius = 25
        tableView.layer.cornerRadius = 25
        getData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        storeData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: preparing the array of players for transfer to PlayGameView
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(rows.isEmpty){
            let alert = UIAlertController(title: "No players found", message: "Please enter player names", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {[weak alert] (_) in
                self.viewDidLoad()
            }))
            self.present(alert, animated: true, completion: nil)
        }
        if segue.identifier == "ThirdViewController" {
            if let destination = segue.destination as? ThirdViewController {
                print("Size of array before passing it: ", rows.count)
                destination.array = rows
            }
        }
    }
    
}

//MARK: Extensions for AddPlayerView
extension AddPlayerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cells", for: indexPath)
        cell.textLabel?.text = rows[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            rows.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        } else if editingStyle == .insert {
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = UIColor.black
        let headerLabel = UILabel(frame: CGRect(x: -55, y: 0, width: headerView.frame.width, height: 30))
        headerLabel.text = "Players"
        headerLabel.textColor = UIColor.white
        headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
        headerLabel.textAlignment = .center
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    
    @IBAction func addbtnPressed(_ sender: Any) {
        addCell()
    }
    //MARK: Adds a row with the name for each added player
    func addCell() {
        let alert = UIAlertController(title: "Add Player", message: "", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "player name"
        }
        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { [weak alert] (_) in
            let row = alert?.textFields![0]
            self.rows.append((row!.text)!)
            self.tableView.reloadData()
        }))
        self.present(alert, animated: true, completion: nil)
        storeData()
    }
    //MARK: stores and retrieves the players added
    func storeData() {
        defaults?.set(rows, forKey: "savedData")
        defaults?.synchronize()
    }
    
    func getData() {
        let data = defaults?.value(forKey: "savedData")
        if data != nil {
            rows = data as! [String]
        } else {}
    }
    
}
