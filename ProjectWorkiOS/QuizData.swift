//
//  QuizData.swift
//  ProjectWorkiOS
//
//  Created by Johannes Hou Gustafsson on 2018-11-25.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import Foundation


class QuizData: Codable {
    
    var question: String
    var correctAnswer: String
    var incorrectAnswer: [String]
    
    private enum CodingKeys: String, CodingKey {
        case question
        case correctAnswer = "correct_answer"
        case incorrectAnswer = "incorrect_answers"
    }
    
    init(question: String = "", correctAnswer: String = "", incorrectAnswer: [String] = [""]) {
        self.question = question
        self.correctAnswer = correctAnswer
        self.incorrectAnswer = incorrectAnswer
    }
}
