//
//  players.swift
//  ProjectWorkiOS
//
//  Created by Johannes Hou Gustafsson on 2018-11-09.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import Foundation

class player {
    var player: String
    var score: Int
    
    init(player: String = "", score: Int = 0) {
        self.player = player
        self.score = score
    }
}
