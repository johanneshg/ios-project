//
//  APIHandler.swift
//  ProjectWorkiOS
//
//  Created by Johannes Hou Gustafsson on 2018-11-25.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import Foundation

class APIHandler {
    
    static public var tempQuiz = QuizResponse()
    
    static private let API_PATH: String = "https://opentdb.com/api.php?amount=50&type=multiple"
    
    static func getQuestions(){
        guard let url = URL(string: API_PATH) else {
            return
        }
        print("hello")
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {return}
            do {
                let decoder = JSONDecoder()
                
                let quiz = try decoder.decode(QuizResponse.self, from: data)
                tempQuiz = quiz
                
            } catch let err {
                print("Error", err)
            }
            }
            .resume()
    }
    var sendQuiz = tempQuiz
}

