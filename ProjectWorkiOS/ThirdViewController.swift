//
//  ThirdViewController.swift
//  ProjectWorkiOS
//
//  Created by Johannes Hou Gustafsson on 2018-11-09.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    @IBOutlet weak var answer1: UIButton!
    @IBOutlet weak var answer2: UIButton!
    @IBOutlet weak var answer3: UIButton!
    @IBOutlet weak var answer4: UIButton!
    @IBOutlet weak var questionWindow: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var playerLabel: UILabel!
    @IBOutlet weak var totalPotLabel: UILabel!
    @IBOutlet weak var currentScoreLabel: UILabel!
    
    var array = [String]()
    var timerIsOn = false
    var timer = Timer()
    var timeRemaining = 30
    var totalTime = 30
    var timerIsPaused = false
    var currentPlayer = 0
    var currentQuestion = 0
    var totalScore = 0
    var pot = "Pot: "
    var scoreText = "Score: "
    
    var players: [player] = []
    var quiz: QuizResponse = QuizResponse()
    var json: APIHandler = APIHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startGameBox()
        addPlayers()
        
        answer1.layer.cornerRadius = 25
        answer2.layer.cornerRadius = 25
        answer3.layer.cornerRadius = 25
        answer4.layer.cornerRadius = 25
        
        quiz = json.sendQuiz
        updateGameLabels()
        
        let scoreToString = String(players[currentPlayer].score)
        let totalScoreToString = String(totalScore)
        
        
        
        
        let potLabelText = pot + totalScoreToString
        let scoreLabel = scoreText + scoreToString
        
        playerLabel.text = players[currentPlayer].player
        currentScoreLabel.text = scoreText + scoreToString
        totalPotLabel.text = potLabelText
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Initiates answer buttons
    @IBAction func firstAnswerButtonPressed(_ sender: UIButton){
        //check if answer is correct, add points
        stopTimer()
        updateScore(answer1.currentTitle!)
        animateButton(sender, answer1.currentTitle!)
        nextPlayer()
    }
    
    @IBAction func secondAnswerButtonPressed(_ sender: UIButton){
        stopTimer()
        updateScore(answer2.currentTitle!)
        animateButton(sender, answer2.currentTitle!)
        nextPlayer()
    }
    
    @IBAction func thirdAnswerButtonPressed(_ sender: UIButton){
        stopTimer()
        updateScore(answer3.currentTitle!)
        animateButton(sender, answer3.currentTitle!)
        nextPlayer()
    }
    
    @IBAction func fourthAnswerButtonPressed(_ sender: UIButton){
        stopTimer()
        updateScore(answer4.currentTitle!)
        animateButton(sender, answer4.currentTitle!)
        nextPlayer()
    }
    
    //MARK: - Starts timer
    func startTimer(){
        if(!timerIsOn){
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
        }
    }
    
    @objc func timerRunning(){
        timeRemaining -= 1
        progressView.setProgress(Float(timeRemaining) / Float(totalTime), animated: true)
        if(timeRemaining == 0){
            stopTimer()
            updateScore("Time ran out")
            nextPlayer()
        }
    }

    //Stops and resets timer
    func stopTimer(){
        timerIsOn = false
        timer.invalidate()
        timeRemaining = totalTime
    }
    
    //MARK: - Pop up window for starting game
    func startGameBox(){
        
        let alert = UIAlertController(title: "Player To Start:", message: array[0], preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Start", style: .default, handler: { [weak alert] (_) in
        self.startTimer()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Fetches data from API
    func fetchQuestion(_ i: Int) -> String {
        return quiz.results[i].question
    }
    
    func fetchCorrectAnswer(_ i: Int) -> String {
        return quiz.results[i].correctAnswer
    }
    
    func fetchIncorrectAnswers(_ i: Int) -> [String]{
        return quiz.results[i].incorrectAnswer
    }
    
    //MARK: Update Score
    func updateScore(_ sender: String){
        if sender == quiz.results[currentQuestion-1].correctAnswer{
            players[currentPlayer].score += 1
            totalScore += 1
            if (players[currentPlayer].score == 10) {
                endGame()
            }
        }
        else{
            players[currentPlayer].score -= totalScore
            if players[currentPlayer].score < 0{
                players[currentPlayer].score = 0
            }
            totalScore = 0
        }
    }
    //MARK: Update of buttons and question label
    func updateGameLabels() {
        if(currentQuestion == 49){
            APIHandler.getQuestions()
            currentQuestion = 0
        }
        
        if(!timerIsOn) {
            let randomNum = Int.random(in: 0...3)
            
            switch randomNum {
            case 0:
                answer1.setTitle(replaceCharacter(fetchCorrectAnswer(currentQuestion)), for: .normal)
                answer2.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[0]), for: .normal)
                answer3.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[1]), for: .normal)
                answer4.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[2]), for: .normal)
            case 1:
                answer2.setTitle(fetchCorrectAnswer(currentQuestion), for: .normal)
                answer1.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[0]), for: .normal)
                answer3.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[1]), for: .normal)
                answer4.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[2]), for: .normal)
            case 2:
                answer3.setTitle(fetchCorrectAnswer(currentQuestion), for: .normal)
                answer1.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[0]), for: .normal)
                answer2.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[1]), for: .normal)
                answer4.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[2]), for: .normal)
            case 3:
                answer4.setTitle(fetchCorrectAnswer(currentQuestion), for: .normal)
                answer1.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[0]), for: .normal)
                answer2.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[1]), for: .normal)
                answer3.setTitle(replaceCharacter(fetchIncorrectAnswers(currentQuestion)[2]), for: .normal)
            default:
                print("Error")
            }
            questionWindow.text = replaceCharacter(fetchQuestion(currentQuestion))
            nextQuestion()
        }
        
    }
    
    //MARK: Decoding of characters
    func replaceCharacter(_ x: String) -> String {
        let temp1 = x
        let temp2 = temp1.replacingOccurrences(of: "&#039;", with: "'")
        let temp3 = temp2.replacingOccurrences(of: "&quot;", with: "\"")
        let temp4 = temp3.replacingOccurrences(of: "&amp;", with: "&")
        let temp5 = temp4.replacingOccurrences(of: "&ldquo;", with: "“")
        let temp6 = temp5.replacingOccurrences(of: "&rdquo;", with: "”")
        return temp6
    }
    
    // MARK: Switch to next player
    func nextPlayer(){
        currentPlayer += 1
        if(currentPlayer == array.count){
            currentPlayer = 0
        }
        let scoreToString = String(players[currentPlayer].score)
        let totalScoreToString = String(totalScore)
        
        playerLabel.text = players[currentPlayer].player
        
        let potLabelText = pot + totalScoreToString
        totalPotLabel.text = potLabelText
        
        let totalScoreText = scoreText + scoreToString
        currentScoreLabel.text = totalScoreText
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.alertAfterQuestion()
            self.updateGameLabels()
        })

    }
    
    //MARK: Switches to the next question
    func nextQuestion(){
        currentQuestion += 1
    }
    
    
    //MARK: Add players to game
    func addPlayers(){
        for name in array{
            players.append(player(player: name))
        }
    }
    
    //MARK: Show next player
    func alertAfterQuestion(){
        let alert = UIAlertController(title: "Next Player", message: players[currentPlayer].player + "\nCurrent Score: " + "\(players[currentPlayer].score)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {[weak alert]
            (_) in
            self.startTimer()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: End game condition
    func endGame(){
        var winners = [Int]()
        var currentLeader = players[0]
        for i in 0..<players.count{
            if (players[i].score > currentLeader.score){
                currentLeader = players[i]
                winners = [i]
            }
            else if (players[i].score == currentLeader.score){
                winners.append(i)
            }
        }
        print(winners.count)
        if (winners.count == 1){
            let alert = UIAlertController(title: "End Result", message: "The winner is " + players[winners[0]].player + " with the score: " +
                "\(players[winners[0]].score)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "End Game", style: .default, handler: {[weak alert] (_) in self.terminateGame()}))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            var winnerNames = String()
            for name in winners{
                if(name+1 < winners.count){
                    winnerNames += players[winners[name]].player + ", "
                }
                else{
                    winnerNames += players[winners[name]].player + " "
                }
            }
            let alert = UIAlertController(title: "End Result", message: "The winners are " + winnerNames + "with the score: " + "\(players[winners[0]].score)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "End Game", style: .default, handler: {[weak alert] (_) in self.terminateGame()}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Exits game
    func terminateGame(){
        APIHandler.getQuestions()
        self.navigationController?.popViewController(animated: true)
    }
    func animateButton(_ senderBtn: UIButton, _ senderStr: String) {

        if senderStr == quiz.results[currentQuestion-1].correctAnswer {
            senderBtn.backgroundColor = UIColor.green
            senderBtn.alpha = 0
            UIView.animate(withDuration: 0.8, animations: {
                senderBtn.alpha = 1
            }, completion: { completed in
                if completed {
                    senderBtn.backgroundColor = UIColor.init(red: (15.0/255.0), green: (55.0/255.0), blue: (57.0/255.0), alpha: 1)
                }
            })
        } else {
            senderBtn.backgroundColor = UIColor.red
            senderBtn.alpha = 0
            UIView.animate(withDuration: 0.8, animations: {
                senderBtn.alpha = 1
            }, completion: { completed in
                if completed {
                    senderBtn.backgroundColor = UIColor.init(red: (15.0/255.0), green: (55.0/255.0), blue: (57.0/255.0), alpha: 1)
                }
            })
        }
    }
        
}

