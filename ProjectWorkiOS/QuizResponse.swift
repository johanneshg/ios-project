//
//  QuizResponse.swift
//  ProjectWorkiOS
//
//  Created by Johannes Hou Gustafsson on 2018-11-25.
//  Copyright © 2018 Johannes Hou Gustafsson. All rights reserved.
//

import Foundation

struct QuizResponse: Codable {
    var results: [QuizData]

    init(result: QuizData = QuizData()) {
        self.results = [result]
    }
}
